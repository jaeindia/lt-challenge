import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Scanner;

public class routeFinder {
	// Compute all the possible routes from source
	static void computeRoutes(Vertex source) {
		source.minDistance = 0;
		PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
		vertexQueue.add(source);

		while (!vertexQueue.isEmpty()) {
			Vertex u = vertexQueue.poll();

			// Visit each edge via u
			for (Edge e : u.adjacencies) {
				Vertex v = e.target;
				int weight = e.weight;
				int distanceThroughU = u.minDistance + weight;

				if (distanceThroughU < v.minDistance) {
					vertexQueue.remove(v);

					v.minDistance = distanceThroughU;
					v.previous = u;
					vertexQueue.add(v);
				}
			}
		}
	}

	// Compute min path to target from source
	static List<Vertex> getMinPath(Vertex target) {
		List<Vertex> path = new ArrayList<Vertex>();

		for (Vertex vertex = target; vertex != null; vertex = vertex.previous)
			path.add(vertex);

		Collections.reverse(path);

		return path;
	}

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		String line;

		HashMap<String, Vertex> station = new HashMap<String, Vertex>(); // Vertex
																			// store
		HashMap<String, HashMap<String, Integer>> stationAdjNodes = new HashMap<String, HashMap<String, Integer>>(); // Neighbour
																														// Vertices
																														// Store

		// Parse Input
		while (reader.hasNext()) {
			line = reader.nextLine();

			if (line.equalsIgnoreCase("end"))
				break;

			String[] lineArr = line.split("\\s+");
			if (!station.containsKey(lineArr[0])) {
				station.put(lineArr[0], new Vertex(lineArr[0]));
				stationAdjNodes.put(lineArr[0], new HashMap<String, Integer>());
			}

			if (!station.containsKey(lineArr[1])) {
				station.put(lineArr[1], new Vertex(lineArr[1]));
				stationAdjNodes.put(lineArr[1], new HashMap<String, Integer>());
			}

			stationAdjNodes.get(lineArr[0]).put(lineArr[1], Integer.parseInt(lineArr[2]));
		}

		int i;
		for (String source : stationAdjNodes.keySet()) {
			station.get(source).adjacencies = new Edge[stationAdjNodes.get(source).keySet().size()];
			i = 0;
			for (String target : stationAdjNodes.get(source).keySet()) {
				station.get(source).adjacencies[i++] = new Edge(station.get(target),
						stationAdjNodes.get(source).get(target));
			}
		}

		String source;
		String target;
		System.out.println("\nEnter Station 1 ...\t");
		source = reader.nextLine();
		System.out.println("\nEnter Station 2 ...\t");
		target = reader.nextLine();

		computeRoutes(station.get(source)); // Compute paths using Dijkstra
		List<Vertex> path = getMinPath(station.get(target)); // Get the min path
		if (path.isEmpty()) {
			System.out.println("\nNo possible path to " + target + " from " + source);
		} else {
			System.out.println("\nPath: " + path + "\n");
			System.out.println("Distance to " + station.get(target) + ": " + station.get(target).minDistance);
		}
		reader.close();
	}
}
