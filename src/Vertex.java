class Vertex implements Comparable<Vertex> {
	final String name;
	Edge[] adjacencies;
	int minDistance = Integer.MAX_VALUE;
	Vertex previous;

	Vertex(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public int compareTo(Vertex neighbour) {
		return Double.compare(minDistance, neighbour.minDistance);
	}
}