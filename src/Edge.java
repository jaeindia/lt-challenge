class Edge {
	final Vertex target;
	final int weight;

	public Edge(Vertex target, int weight) {
		this.target = target;
		this.weight = weight;
	}
}