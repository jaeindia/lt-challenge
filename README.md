# Leo Tech - Challenge

How to Run:

```
java routeFinder.java
```

Sample Run 1:

```
A B 3
B A 3
A D 6
B C 7
C D 8
D E 9
E D 9
D C 9
D B 5
C E 3
End

Enter Station 1 ...	
A

Enter Station 2 ...	
E

Path: [A, B, C, E]

Distance to E: 13
```

Sample Run 2:

```
A B 3
B A 3
A D 6
B C 7
C D 8
D E 9
E D 9
D C 9
D B 5
C E 3
End

Enter Station 1 ...	
A

Enter Station 2 ...	
Z

No possible path to Z from A
```